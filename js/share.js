let shareWord = "性价比超高的蓝牙点读笔，搭配的图书也非常好，强烈推荐购买，復·制这段描述 {0} 后到淘♂寳♀优惠码 {1} 发给客服领优惠券";


function initStyle() {
    var img_height=$('#quan').height();
    $('.shareContent').css({
        "height":img_height+'px'
    })
}
initStyle();

function sharedApp(title,desc,url){
    var u = navigator.userAgent;
    var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
    if (isiOS) {
          try {
              window.webkit.messageHandlers.ios.postMessage({"body": "{\"title\":\""+title+"\",\"desc\":\""+desc+"\",\"url\":\""+url+"\"}"});
          } catch (error) {
               console.log(error)
          }
     } else if (isAndroid) {
           window.android.shared(title,desc,url);
     }
}


$(function () {
    $('#showTerm').attr("href",`./shareterm.html?token=${UrlParm.parm("token")}`);
    $('#shareBtn').click(()=>{
        sharedApp("分享赚积分喽",shareWord);
    });

    try {
        $.ajax({
            url:SHARE_QUERY_URL,
            type:"POST",
            data:{
                token:decodeURI(UrlParm.parm("token"))
            },
            success: data =>{
                body = data.body;
                if(!body)return;
                
                $("#video1").attr("src",body.videoUrl);
                $("#coupon").html(body.coupon);
                $("#shareCode").html(body.shareCode);
                $("#integral").html(body.integral);
                shareWord = body.world?body.world:shareWord;
                shareWord = shareWord.replace('{0}',body.url).replace('{1}',body.shareCode);
            }
        }); 
    } catch (error) {
        console.log(error);
    }
    
    try {
        $.ajax({
            url:SHARE_SHOW_URL,
            type:"POST",
            data:{
                token:decodeURI(UrlParm.parm("token"))
            },
            success: data =>{
                body = data.body;
                if(!body)return;

                let content = data.body.content;
                let lines = content.split("\n");
                let show = "";
                lines.forEach(line =>{
                    show += `<li>${line}</li>`
                });
                $("#con1").html(show);
                marqueeScroll();
            }
        });
    } catch (error) {
        console.log(error);
    }
    
});



// 滚动的文字
function marqueeScroll() {
    var area =document.getElementById('scrollBox');
    var con1 = document.getElementById('con1');
    var con2 = document.getElementById('con2');
    con2.innerHTML=con1.innerHTML;
    function scrollUp(){
        if(area.scrollTop>=con1.offsetHeight){
            area.scrollTop=0;
        }else{
            area.scrollTop++
        }
    }
    var time = 30;
    var mytimer=setInterval(scrollUp,time);
    // area.onmouseover=function(){
    //     clearInterval(mytimer);
    // }
    // area.onmouseout=function(){
    //     mytimer=setInterval(scrollUp,time);
    // }
}