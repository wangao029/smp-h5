// const ROOT_URL = "http://localhost:8001";
const ROOT_URL = "";
const AWARD_QUERY_URL = `${ROOT_URL}/client/lotteryCase/query`;
const AWARD_SHOW_QUERY_URL = `${ROOT_URL}/client/lotteryCase/show/query`;
const AWARD_DRAW_URL = `${ROOT_URL}/client/lotteryCase/draw`;
const AWARD_TERM_URL = `${ROOT_URL}/client/lotteryCase/term/query`;
const WINNER_SHOW_URL = `${ROOT_URL}/client/lotteryCase/winner/query`;

const SHARE_QUERY_URL = `${ROOT_URL}/client/shareCase/query`;
const SHARE_TERM_URL = `${ROOT_URL}/client/shareCase/term/query`;
const SHARE_SHOW_URL = `${ROOT_URL}/client/shareCase/show/query`;


UrlParm = function () { // url参数
    var data, index;
    (function init() {
        data = [];
        index = {};
        var u = window.location.search.substr(1);
        if (u != '') {
            var parms = decodeURIComponent(u).split('&');
            for (var i = 0, len = parms.length; i < len; i++) {
                if (parms[i] != '') {
                    var p = parms[i].split("=");
                    if (p.length == 1 || (p.length == 2 && p[1] == '')) {
                        data.push(['']);
                        index[p[0]] = data.length - 1;
                    } else if (typeof (p[0]) == 'undefined' || p[0] == '') {
                        data[0] = [p[1]];
                    } else if (typeof (index[p[0]]) == 'undefined') { // c=aaa
                        data.push([p[1]]);
                        index[p[0]] = data.length - 1;
                    } else {// c=aaa
                        data[index[p[0]]].push(p[1]);
                    }
                }
            }
        }
    })();
    return {
        // 获得参数,类似request.getParameter()
        parm: function (o) { // o: 参数名或者参数次序
            try {
                return (typeof (o) == 'number' ? data[o][0] : data[index[o]][0]);
            } catch (e) {
            }
        },
        //获得参数组, 类似request.getParameterValues()
        parmValues: function (o) { //  o: 参数名或者参数次序
            try {
                return (typeof (o) == 'number' ? data[o] : data[index[o]]);
            } catch (e) { }
        },
        //是否含有parmName参数
        hasParm: function (parmName) {
            return typeof (parmName) == 'string' ? typeof (index[parmName]) != 'undefined' : false;
        },
        // 获得参数Map ,类似request.getParameterMap()
        parmMap: function () {
            var map = {};
            try {
                for (var p in index) { map[p] = data[index[p]]; }
            } catch (e) { }
            return map;
        }
    }
} ();