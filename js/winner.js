let winner = "<ul>";
$(function () {
    $.ajax({
        url: WINNER_SHOW_URL,
        type: "POST",
        data: {
            token: decodeURI(UrlParm.parm("token")),
        },
        success: data => {
            let {body={}} = data;
            let {content="暂无中奖用户"} = body;
            content.split("\n").map(name =>{
                winner += `<li class='bs-callout bs-callout-running'>${name}</li>`;
            });
            winner += '</ul>';
            $("#winners").html(winner);
        }
    });
});

