let award_id = null;
let winner = '<ul>';
let isActivate = null;

// 初始化样式
function initStyle() {
    var innerContent_Width = $('#innerContent').width();
    $('#innerContent').css({
        "height": innerContent_Width + 'px'
    });
}
initStyle();

$(function () {
    $('#read').attr("href", `./awardterm.html?token=${UrlParm.parm("token")}`);
    $('#winnerBtn').attr("href", `./winner.html?token=${UrlParm.parm("token")}`);
    isActivate = UrlParm.parm("isActivate");

    try {
        $.ajax({
            url: AWARD_QUERY_URL,
            type: "POST",
            data: {
                token: decodeURI(UrlParm.parm("token"))
            },
            success: data => {
                body = data.body;
                if (!body) {
                    $("#img").attr("src", "./image/timg.jpg");
                    return;
                };

                award_id = body.id;
                $("#img").attr("src", body.url);
                $("#num").html(body.num);
                $("#endTime").html(body.endTime.substr(0, 10));
                $("#title1").html(body.name);
            }
        });
    } catch (err) {
        console.log(err);
    }


    try {
        $.ajax({
            url: AWARD_SHOW_QUERY_URL,
            type: "POST",
            data: {
                token: decodeURI(UrlParm.parm("token"))
            },
            success: data => {
                if (!data.body) return;
                let content = data.body.content;
                let lines = content.split("\n");

                let show = "";
                lines.forEach(line => {
                    show += `<li>${line}</li>`
                });
                $("#con1").html(show);
                marqueeScroll();
            }
        });
    } catch (error) {
        console.log(error);
    }


    $("#award_btn").click(() => {
        if(isActivate!=1){
            alert("NO_ACTIVATE");
            return;
        }
        if (award_id == null) {
            alert("请等待活动开始哦");
            return;
        }
        $.ajax({
            url: AWARD_DRAW_URL,
            type: "POST",
            data: {
                token: decodeURI(UrlParm.parm("token")),
                id: award_id
            },
            success: data => {
                alert(data.info);
            }
        });
    });

    // $.ajax({
    //     url: WINNER_SHOW_URL,
    //     type: "POST",
    //     data: {
    //         token: decodeURI(UrlParm.parm("token")),
    //     },
    //     success: data => {
    //         let {body={}} = data;
    //         let {content="暂无中奖用户"} = body;
    //         content.split("\n").map(name =>{
    //             winner += `<li>${name}</li>`;
    //         });
    //         winner += '</ul>';
    //     }
    // });

    // $("#winnerBtn").click(() => {
    //     winnerBtn
    //     // $('body').dialog({
    //     //     title:'中奖人员名单',
    //     //     type: 'success',
    //     //     showBoxShadow: true,
    //     //     duration: 0,
    //     //     buttons: [{
    //     //         name: '确定',
    //     //         className: 'false'
    //     //     }],
    //     //     discription: winner?winner:"暂无中奖用户",
    //     //     buttonsSameWidth: true
    //     // });
    // });
});


// 滚动的文字
function marqueeScroll() {
    var area = document.getElementById('scrollBox');
    var con1 = document.getElementById('con1');
    var con2 = document.getElementById('con2');
    con2.innerHTML = con1.innerHTML;

    function scrollUp() {
        if (area.scrollTop >= con1.offsetHeight) {
            area.scrollTop = 0;
        } else {
            area.scrollTop++
        }
    }
    var time = 30;
    var mytimer = setInterval(scrollUp, time);
    // area.onmouseover = function () {
    //     clearInterval(mytimer);
    // }
    // area.onmouseout = function () {
    //     mytimer = setInterval(scrollUp, time);
    // }
}